export default interface Url {
  id: string;
  originalUrl: string;
  tinyUrl: string;
  validUntil: string;
}
