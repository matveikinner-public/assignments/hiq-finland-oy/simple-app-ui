import { Observable } from "rxjs";
import Url from "../models/url.model";

interface ShortenerRepository {
  getUrls(): Observable<Url[]>;
  createUrl(url: string): Observable<Url>;
}

export default ShortenerRepository;
