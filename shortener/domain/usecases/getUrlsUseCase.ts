import { inject, injectable } from "inversify";
import SHORTENER_BINDINGS from "@shortener/di/shortenerBindings";
import ShortenerRepository from "../repositories/shortenerRepository";

@injectable()
class GetUrlsUseCase {
  @inject(SHORTENER_BINDINGS.ShortenerRepository) private shortenerRepository!: ShortenerRepository;

  invoke = () => this.shortenerRepository.getUrls();
}

export default GetUrlsUseCase;
