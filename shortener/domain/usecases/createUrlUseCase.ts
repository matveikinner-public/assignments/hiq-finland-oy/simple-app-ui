import { inject, injectable } from "inversify";
import SHORTENER_BINDINGS from "@shortener/di/shortenerBindings";
import ShortenerRepository from "../repositories/shortenerRepository";

@injectable()
class CreateUrlUseCase {
  @inject(SHORTENER_BINDINGS.ShortenerRepository) private shortenerRepository!: ShortenerRepository;

  invoke = (url: string) => this.shortenerRepository.createUrl(url);
}

export default CreateUrlUseCase;
