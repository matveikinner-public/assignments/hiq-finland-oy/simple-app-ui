import { inject, injectable } from "inversify";
import ShortenerRepository from "@shortener/domain/repositories/shortenerRepository";
import SHORTENER_BINDINGS from "@shortener/di/shortenerBindings";

@injectable()
class ShortenerRepositoryImpl implements ShortenerRepository {
  @inject(SHORTENER_BINDINGS.RemoteShortenerRepository) private remoteShortenerRepository!: ShortenerRepository;

  getUrls = () => this.remoteShortenerRepository.getUrls();
  createUrl = (url: string) => this.remoteShortenerRepository.createUrl(url);
}

export default ShortenerRepositoryImpl;
