import { inject, injectable } from "inversify";
import { map } from "rxjs";
import ShortenerRepository from "@shortener/domain/repositories/shortenerRepository";
import SHORTENER_BINDINGS from "@shortener/di/shortenerBindings";
import ShortenerApi from "../network/api/shortenerApi";
import { mapToUrl } from "../network/mappers/urlsMapper";

@injectable()
class RemoteShortenerRepository implements ShortenerRepository {
  @inject(SHORTENER_BINDINGS.ShortenerApi) private shortenerApi!: ShortenerApi;

  getUrls = () => this.shortenerApi.getUrls().pipe(map((response) => response.data.map((url) => mapToUrl(url))));
  createUrl = (url: string) => this.shortenerApi.createUrl(url).pipe(map((response) => mapToUrl(response.data)));
}

export default RemoteShortenerRepository;
