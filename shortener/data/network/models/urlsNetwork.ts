namespace UrlsNetwork {
  export interface GetResponse {
    statusCode: number;
    data: Url[];
  }

  export interface PostResponse {
    statusCode: number;
    data: Url;
  }

  export interface Url {
    id: string;
    originalUrl: string;
    tinyUrl: string;
    validUntil: string;
    createdAt: string;
    updatedAt: string;
  }
}

export default UrlsNetwork;
