import { inject, injectable } from "inversify";
import { from, map, Observable } from "rxjs";
import HttpService from "@core/data/network/services/http.service";
import CORE_BINDINGS from "@core/di/coreBindings";
import SHORTENER_RESOURCE_URLS from "./shortenerResourceUrls";
import UrlsNetwork from "../models/urlsNetwork";

@injectable()
class ShortenerApi {
  @inject(CORE_BINDINGS.HttpService) private httpService!: HttpService;

  getUrls = (): Observable<UrlsNetwork.GetResponse> =>
    from(this.httpService.httpClient.get<UrlsNetwork.GetResponse>(SHORTENER_RESOURCE_URLS.CRUD.URLS)).pipe(
      map((response) => response.data)
    );

  createUrl = (url: string): Observable<UrlsNetwork.PostResponse> =>
    from(
      this.httpService.httpClient.post<UrlsNetwork.PostResponse>(
        `${SHORTENER_RESOURCE_URLS.CRUD.URLS}/${encodeURIComponent(url)}`
      )
    ).pipe(map((response) => response.data));
}

export default ShortenerApi;
