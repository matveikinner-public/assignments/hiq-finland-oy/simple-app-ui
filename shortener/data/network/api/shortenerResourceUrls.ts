namespace SHORTENER_RESOURCE_URLS {
  export enum GET {}

  export enum POST {}

  export enum PUT {}

  export enum DELETE {}

  export enum CRUD {
    URLS = "api/v1/converter",
  }
}

export default SHORTENER_RESOURCE_URLS;
