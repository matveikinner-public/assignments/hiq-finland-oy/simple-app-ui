import Url from "@shortener/domain/models/url.model";
import UrlsNetwork from "../models/urlsNetwork";

export const mapToUrl = (url: UrlsNetwork.Url): Url => ({
  id: url.id,
  originalUrl: url.originalUrl,
  tinyUrl: url.tinyUrl,
  validUntil: url.validUntil,
});
