import React, { FunctionComponent } from "react";
import { useDispatch } from "react-redux";
import { Box, Button, TextField, Typography } from "@mui/material";
import { LiteralUnion, RegisterOptions, useForm } from "react-hook-form";
import * as R from "ramda";
import validator from "validator";
import { createUrlRequest } from "@shortener/ui/adapters/redux/urls/urls.actions";

const Create: FunctionComponent = () => {
  const dispatch = useDispatch();
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<{ url: string }>();

  const handleCreate = (data: { url: string }) => {
    reset({ url: "" }, { keepValues: false });
    dispatch(createUrlRequest(data.url));
  };

  const displayError = (errorType: LiteralUnion<keyof RegisterOptions, string>): string => {
    switch (errorType) {
      case "required":
        return "The URL cannot be empty";
      case "validate":
        return "The URL must be FQDN or PQDN";
      default:
        return "Validation error";
    }
  };

  return (
    <Box
      data-testid="form"
      component="form"
      sx={{
        my: 1,
        "& > :not(style)": { my: 1 },
      }}
      noValidate
      autoComplete="off"
    >
      <Typography variant="body1" gutterBottom>
        Please provide a Fully Qualified Domain Name (FQDN) or Partially Qualified Domain Name (PQDN) in order to
        convert it to short url
      </Typography>
      <TextField
        fullWidth
        id="url"
        label="URL"
        variant="outlined"
        {...register("url", {
          required: true,
          validate: (input) => validator.isURL(input),
        })}
        helperText={errors.url ? displayError(errors.url.type) : null}
        error={!R.isNil(errors.url)}
      />
      <Button variant="outlined" sx={{ mr: 1 }} onClick={handleSubmit(handleCreate)}>
        Create
      </Button>
    </Box>
  );
};

export default Create;
