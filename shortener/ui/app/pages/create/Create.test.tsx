import React from "react";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import * as reactRedux from "react-redux";
import CoreProvider from "@core/ui/app/CoreProvider";
import Create from "./Create";
import { createUrlRequest } from "@shortener/ui/adapters/redux/urls/urls.actions";

describe("Create Page", () => {
  const useDispatchMock = jest.spyOn(reactRedux, "useDispatch");
  const mockDispatch = jest.fn();
  useDispatchMock.mockReturnValue(mockDispatch);

  beforeEach(() => {
    useDispatchMock.mockClear();
  });

  it("renders form", () => {
    // Arrange
    render(<Create />, { wrapper: CoreProvider });
    const formElement = screen.getByTestId("form");

    // Assert
    expect(formElement).toBeInTheDocument();
  });

  it("renders form fields", () => {
    // Arrange
    render(<Create />, { wrapper: CoreProvider });
    const textFieldElement = screen.getByRole("textbox");

    // Assert
    expect(textFieldElement).toBeInTheDocument();
  });

  it("renders submit button", () => {
    // Arrange
    render(<Create />, { wrapper: CoreProvider });
    const buttonElement = screen.getByRole("button", { name: /create/i });

    // Assert
    expect(buttonElement).toBeInTheDocument();
  });

  it("should validate form fields for required", async () => {
    render(<Create />, { wrapper: CoreProvider });
    const buttonElement = screen.getByRole("button", { name: /create/i });

    fireEvent.input(screen.getByRole("textbox", { name: /url/i }), {
      target: {
        value: "",
      },
    });

    fireEvent.click(buttonElement);

    expect(await screen.findByText(/the url cannot be empty/i)).toBeInTheDocument();
    expect(mockDispatch).not.toHaveBeenCalled();
  });

  it("should validate form fields for pattern", async () => {
    render(<Create />, { wrapper: CoreProvider });
    const buttonElement = screen.getByRole("button", { name: /create/i });

    fireEvent.input(screen.getByRole("textbox", { name: /url/i }), {
      target: {
        value: "Lorem",
      },
    });

    fireEvent.click(buttonElement);

    expect(await screen.findByText(/the url must be fqdn or pqdn/i)).toBeInTheDocument();
    expect(mockDispatch).not.toHaveBeenCalled();
  });

  it("should submit form", async () => {
    render(<Create />, { wrapper: CoreProvider });
    const buttonElement = screen.getByRole("button", { name: /create/i });

    fireEvent.input(screen.getByRole("textbox", { name: /url/i }), {
      target: {
        value: "https://hiqfinland.fi/avoimet-tyopaikat/",
      },
    });

    fireEvent.click(buttonElement);

    await waitFor(() =>
      expect(mockDispatch).toHaveBeenCalledWith(createUrlRequest("https://hiqfinland.fi/avoimet-tyopaikat/"))
    );
  });
});
