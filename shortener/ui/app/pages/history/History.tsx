import React, { FunctionComponent, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Box, List, Typography } from "@mui/material";
import moment from "moment";
import { getUrlsRequest } from "@shortener/ui/adapters/redux/urls/urls.actions";
import { selectUrls } from "@shortener/ui/adapters/redux/urls/urls.selectors";
import UrlItem from "./components/urlItem/UrlItem";

const History: FunctionComponent = () => {
  const dispatch = useDispatch();
  const urls = useSelector(selectUrls);

  useEffect(() => {
    dispatch(getUrlsRequest());
  }, []);

  if (urls.length) {
    return (
      <List>
        {urls
          .sort((a, b) => moment(b.validUntil).diff(moment(a.validUntil)))
          .map((url) => (
            <UrlItem key={url.id} {...url} />
          ))}
      </List>
    );
  }

  return (
    <Box
      sx={{
        my: 1,
      }}
    >
      <Typography variant="body1" gutterBottom>
        There is no history. Please create a url redirect to view history
      </Typography>
    </Box>
  );
};

export default History;
