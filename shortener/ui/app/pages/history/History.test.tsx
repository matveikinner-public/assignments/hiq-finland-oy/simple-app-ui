import React from "react";
import { render, waitFor } from "@testing-library/react";
import * as reactRedux from "react-redux";
import CoreProvider from "@core/ui/app/CoreProvider";
import History from "./History";
import { getUrlsRequest } from "@shortener/ui/adapters/redux/urls/urls.actions";

describe("History Page", () => {
  const useDispatchMock = jest.spyOn(reactRedux, "useDispatch");
  const mockDispatch = jest.fn();
  useDispatchMock.mockReturnValue(mockDispatch);

  it("should fetch data", async () => {
    // Arrange
    render(<History />, { wrapper: CoreProvider });

    // Assert
    await waitFor(() => expect(mockDispatch).toHaveBeenCalledWith(getUrlsRequest()));
  });
});
