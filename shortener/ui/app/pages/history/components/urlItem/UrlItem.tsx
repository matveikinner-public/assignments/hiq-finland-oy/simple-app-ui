import React, { FunctionComponent } from "react";
import { ListItem, Paper, Typography, Link } from "@mui/material";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
import moment from "moment";
import Url from "@shortener/domain/models/url.model";
import { TypographyContent } from "./UrlItem.styles";

const UrlItem: FunctionComponent<Url> = ({ id, originalUrl, tinyUrl, validUntil }: Url) => {
  const currentTime = moment();
  const expirationTime = moment(validUntil);
  const expirationDuration = moment.duration(expirationTime.diff(currentTime));

  const expirationDays = expirationDuration.days();
  expirationDuration.subtract(expirationDays);

  const expirationHours = expirationDuration.hours();
  expirationDuration.subtract(expirationHours);

  const expirationMinutes = expirationDuration.minutes();

  return (
    <Paper key={id} elevation={1} sx={{ mb: 2 }}>
      <ListItem alignItems="flex-start" sx={{ flexDirection: "column" }}>
        <Typography variant="overline" display="block">
          Original URL
        </Typography>
        <TypographyContent variant="body1" gutterBottom sx={{ marginTop: `-${2}` }}>
          <Link rel="noopener noreferrer" href={originalUrl} target="_blank">
            {originalUrl}
          </Link>
        </TypographyContent>
        <Typography variant="overline" display="block">
          Short URL
        </Typography>
        <TypographyContent variant="body1" gutterBottom>
          <Link rel="noopener noreferrer" href={tinyUrl} target="_blank">
            {tinyUrl}
          </Link>
        </TypographyContent>
        <Typography variant="caption" display="block" sx={{ display: "flex", alignItems: "center" }}>
          <AccessTimeIcon sx={{ mr: 1 }} />
          {`Expires in ${expirationDays} day(s) ${expirationHours} hour(s) and ${expirationMinutes} minute(s)`}
        </Typography>
      </ListItem>
    </Paper>
  );
};

export default UrlItem;
