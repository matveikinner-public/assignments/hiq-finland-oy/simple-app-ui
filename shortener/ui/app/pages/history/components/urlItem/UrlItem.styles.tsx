import { styled } from "@mui/system";
import { Typography } from "@mui/material";

export const TypographyContent = styled(Typography)(({ theme }) => ({
  marginTop: `-${theme.spacing(1)}`,
}));
