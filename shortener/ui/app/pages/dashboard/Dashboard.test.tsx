import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import Dashboard from "./Dashboard";
import CoreProvider from "@core/ui/app/CoreProvider";

describe("Dashboard", () => {
  it("has exactly two (2) tabs", () => {
    // Arrange
    render(<Dashboard />, { wrapper: CoreProvider });
    const tabElements = screen.getAllByRole("tab");

    // Assert
    tabElements.forEach((tabElement) => expect(tabElement).toBeInTheDocument());
    expect(tabElements).toHaveLength(2);
  });

  it("has first tab active", () => {
    // Arrange
    render(<Dashboard />, { wrapper: CoreProvider });
    const tabElementSelected = screen.getByRole("tab", { selected: true });
    const tabElementNotSelected = screen.getByRole("tab", { name: /history/i });

    // Act
    fireEvent.click(tabElementNotSelected);

    // Assert
    expect(tabElementNotSelected).toHaveAttribute("aria-selected", "true");
    expect(tabElementSelected).toBeInTheDocument();
  });
});
