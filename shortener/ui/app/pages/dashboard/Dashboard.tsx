import React, { FunctionComponent, useState } from "react";
import { Box, Tab, Tabs } from "@mui/material";
import Loader from "@core/ui/components/loader/Loader";
import Toast from "@core/ui/components/toast/Toast";
import TabPanel from "./components/tabPanel/TabPanel";
import History from "../history/History";
import Create from "../create/Create";

const Dashboard: FunctionComponent = () => {
  const [value, setValue] = useState(0);

  const a11yProps = (index: number) => ({
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  });

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  return (
    <>
      <Loader />
      <Toast />
      <Box sx={{ width: "100%" }}>
        <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
          <Tabs value={value} onChange={handleChange} aria-label="tabs">
            <Tab label="Create" {...a11yProps(0)} />
            <Tab label="History" {...a11yProps(1)} />
          </Tabs>
        </Box>
        <TabPanel value={value} index={0}>
          <Create />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <History />
        </TabPanel>
      </Box>
    </>
  );
};

export default Dashboard;
