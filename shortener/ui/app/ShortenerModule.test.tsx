import React from "react";
import { render } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";
import { createMemoryHistory } from "history";
import CoreProvider from "@core/ui/app/CoreProvider";
import ShortenerModule from "./Shortener";

describe("ShortenerModule", () => {
  it("navigates to /", () => {
    render(
      <Router>
        <ShortenerModule />
      </Router>,
      { wrapper: CoreProvider }
    );
    const history = createMemoryHistory();

    expect(history.location.pathname).toBe("/");
  });
});
