import { RouteProps } from "react-router-dom";
import Dashboard from "./pages/dashboard/Dashboard";

const routes: RouteProps[] = [
  {
    path: "/",
    exact: true,
    component: Dashboard,
  },
];

export default routes;
