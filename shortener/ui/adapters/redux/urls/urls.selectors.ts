import { RootState } from "@core/ui/frameworks/redux.config";
import { UrlState } from "./urls.types";

export const selectUrls = (state: RootState): UrlState => state.shortener.urls;
