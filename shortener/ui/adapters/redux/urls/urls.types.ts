import Url from "@shortener/domain/models/url.model";
import {
  CREATE_URL_FAILURE,
  CREATE_URL_REQUEST,
  CREATE_URL_SUCCESS,
  GET_URLS_FAILURE,
  GET_URLS_REQUEST,
  GET_URLS_SUCCESS,
} from "./urls.constants";

export type UrlState = Url[];

export interface GetUrlsRequest {
  type: typeof GET_URLS_REQUEST;
}

export interface GetUrlsSuccesss {
  type: typeof GET_URLS_SUCCESS;
  payload: Url[];
}

export interface GetUrlsFailure {
  type: typeof GET_URLS_FAILURE;
}

export interface CreateUrlRequest {
  type: typeof CREATE_URL_REQUEST;
  payload: string;
}

export interface CreateUrlSuccess {
  type: typeof CREATE_URL_SUCCESS;
  payload: Url;
}

export interface CreateUrlFailure {
  type: typeof CREATE_URL_FAILURE;
}

export type UrlActionTypes =
  | GetUrlsRequest
  | GetUrlsSuccesss
  | GetUrlsFailure
  | CreateUrlRequest
  | CreateUrlSuccess
  | CreateUrlFailure;
