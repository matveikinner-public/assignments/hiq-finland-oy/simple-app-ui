import Url from "@shortener/domain/models/url.model";
import { UrlActionTypes } from "./urls.types";
import {
  CREATE_URL_FAILURE,
  CREATE_URL_REQUEST,
  CREATE_URL_SUCCESS,
  GET_URLS_FAILURE,
  GET_URLS_REQUEST,
  GET_URLS_SUCCESS,
} from "./urls.constants";

const initialState: Url[] = [];

const urlsReducers = (state = initialState, action: UrlActionTypes) => {
  switch (action.type) {
    case GET_URLS_REQUEST:
      return state;
    case GET_URLS_SUCCESS:
      return [...state, ...action.payload].filter((v, i, a) => a.findIndex((t) => t.id === v.id) === i);
    case GET_URLS_FAILURE:
      return state;
    case CREATE_URL_REQUEST:
      return state;
    case CREATE_URL_SUCCESS:
      return [...state, action.payload];
    case CREATE_URL_FAILURE:
      return state;
    default:
      return state;
  }
};

export default urlsReducers;
