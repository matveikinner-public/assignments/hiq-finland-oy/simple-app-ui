import Url from "@shortener/domain/models/url.model";
import {
  CREATE_URL_FAILURE,
  CREATE_URL_REQUEST,
  CREATE_URL_SUCCESS,
  GET_URLS_FAILURE,
  GET_URLS_REQUEST,
  GET_URLS_SUCCESS,
} from "./urls.constants";

export const getUrlsRequest = () => ({
  type: GET_URLS_REQUEST,
});

export const getUrlsSuccess = (payload: Url[]) => ({
  type: GET_URLS_SUCCESS,
  payload,
});

export const getUrlsFailure = () => ({
  type: GET_URLS_FAILURE,
});

export const createUrlRequest = (payload: string) => ({
  type: CREATE_URL_REQUEST,
  payload,
});

export const createUrlSuccess = (payload: Url) => ({
  type: CREATE_URL_SUCCESS,
  payload,
});

export const createUrlFailure = () => ({
  type: CREATE_URL_FAILURE,
});
