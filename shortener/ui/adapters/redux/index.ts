import { combineReducers } from "redux";
import urlsReducer from "./urls/urls.reducer";

export default combineReducers({
  urls: urlsReducer,
});
