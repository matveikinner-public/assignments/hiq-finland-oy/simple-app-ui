import { put } from "@redux-saga/core/effects";
import { lastValueFrom, Observable } from "rxjs";
import { createLoader, removeLoader } from "@core/ui/adapters/redux/loader/loader.actions";
import shortenerContainer from "@shortener/di/shortenerContainer";
import SHORTENER_BINDINGS from "@shortener/di/shortenerBindings";
import CreateUrlUseCase from "@shortener/domain/usecases/createUrlUseCase";
import { CreateUrlRequest } from "../../redux/urls/urls.types";
import Url from "@shortener/domain/models/url.model";
import { createUrlFailure, createUrlSuccess } from "../../redux/urls/urls.actions";
import { createErrorToast, createSuccessToast } from "@core/ui/adapters/redux/toast/toast.actions";

export function* createUrlRequestSaga({ payload }: CreateUrlRequest) {
  yield put(createLoader());
  const createUrlUseCase = shortenerContainer.get<CreateUrlUseCase>(SHORTENER_BINDINGS.CreateUrlUseCase);

  try {
    const url = (yield lastValueFrom((yield createUrlUseCase.invoke(payload)) as Observable<Url>)) as Url;
    yield put(createUrlSuccess(url));
    yield put(createSuccessToast("Success while attempting to create url"));
  } catch (err) {
    yield put(createUrlFailure());
    yield put(createErrorToast("Error while attempting to create url"));
  } finally {
    yield put(removeLoader());
  }
}
