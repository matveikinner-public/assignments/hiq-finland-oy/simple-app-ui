import { put } from "@redux-saga/core/effects";
import { lastValueFrom, Observable } from "rxjs";
import { createLoader, removeLoader } from "@core/ui/adapters/redux/loader/loader.actions";
import { createErrorToast, createSuccessToast } from "@core/ui/adapters/redux/toast/toast.actions";
import shortenerContainer from "@shortener/di/shortenerContainer";
import SHORTENER_BINDINGS from "@shortener/di/shortenerBindings";
import GetUrlsUseCase from "@shortener/domain/usecases/getUrlsUseCase";
import Url from "@shortener/domain/models/url.model";
import { getUrlsFailure, getUrlsSuccess } from "../../redux/urls/urls.actions";

export function* getUrlsRequestSaga() {
  yield put(createLoader());
  const getUrlsUseCase = shortenerContainer.get<GetUrlsUseCase>(SHORTENER_BINDINGS.GetUrlsUseCase);

  try {
    const urls = (yield lastValueFrom((yield getUrlsUseCase.invoke()) as Observable<Url[]>)) as Url[];
    yield put(getUrlsSuccess(urls));
    yield put(createSuccessToast("Success while attempting to fetch urls"));
  } catch (err) {
    yield put(getUrlsFailure());
    yield put(createErrorToast("Error while attempting to fetch books"));
  } finally {
    yield put(removeLoader());
  }
}
