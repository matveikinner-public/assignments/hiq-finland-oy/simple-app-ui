import { all, takeLatest } from "@redux-saga/core/effects";
import { CREATE_URL_REQUEST, GET_URLS_REQUEST } from "../redux/urls/urls.constants";
import { createUrlRequestSaga } from "./urls/createUrlRequestSaga";
import { getUrlsRequestSaga } from "./urls/getUrlsRequestSaga";

export default function* root() {
  yield all([takeLatest(GET_URLS_REQUEST, getUrlsRequestSaga), takeLatest(CREATE_URL_REQUEST, createUrlRequestSaga)]);
}
