import { Container } from "inversify";
import coreContainer from "@core/di/coreContainer";
import SHORTENER_BINDINGS from "./shortenerBindings";
import ShortenerApi from "@shortener/data/network/api/shortenerApi";
import ShortenerRepository from "@shortener/domain/repositories/shortenerRepository";
import ShortenerRepositoryImpl from "@shortener/data/shortenerRepositoryImpl";
import RemoteShortenerRepository from "@shortener/data/remote/remoteShortenerRepository";
import GetUrlsUseCase from "@shortener/domain/usecases/getUrlsUseCase";
import CreateUrlUseCase from "@shortener/domain/usecases/createUrlUseCase";

/**
 * Initialize empty DI container to store Shortener Module bindings
 */
const newContainer = new Container();

/**
 * Create DI container for Shortener Module and merge Core Module DI Container to it, in order to access its implementations
 */
const shortenerContainer = Container.merge(coreContainer, newContainer);

// /**
//  * Bind API to Shortener Module DI Container
//  */
shortenerContainer.bind<ShortenerApi>(SHORTENER_BINDINGS.ShortenerApi).to(ShortenerApi);

// /**
//  * Bind Repository Pattern implementations to Shortener Module DI Container
//  */
shortenerContainer.bind<ShortenerRepository>(SHORTENER_BINDINGS.ShortenerRepository).to(ShortenerRepositoryImpl);
shortenerContainer
  .bind<ShortenerRepository>(SHORTENER_BINDINGS.RemoteShortenerRepository)
  .to(RemoteShortenerRepository);

// /**
//  * Bind Use Cases to Shortener Module DI Container
//  */
shortenerContainer.bind<GetUrlsUseCase>(SHORTENER_BINDINGS.GetUrlsUseCase).to(GetUrlsUseCase);
shortenerContainer.bind<CreateUrlUseCase>(SHORTENER_BINDINGS.CreateUrlUseCase).to(CreateUrlUseCase);

export default shortenerContainer;
