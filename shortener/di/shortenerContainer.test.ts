import shortenerContainer from "./shortenerContainer";
import SHORTENER_BINDINGS from "./shortenerBindings";
import ShortenerApi from "@shortener/data/network/api/shortenerApi";
import ShortenerRepository from "@shortener/domain/repositories/shortenerRepository";
import GetUrlsUseCase from "@shortener/domain/usecases/getUrlsUseCase";
import CreateUrlUseCase from "@shortener/domain/usecases/createUrlUseCase";

describe("Shortener DI Container", () => {
  beforeEach(() => {
    // create a snapshot so each unit test can modify it without breaking other unit tests
    shortenerContainer.snapshot();
  });

  afterEach(() => {
    // Restore to last snapshot so each unit test takes a clean copy of the application container
    shortenerContainer.restore();
  });

  it("binds ShortenerApi", () => {
    expect(shortenerContainer.get<ShortenerApi>(SHORTENER_BINDINGS.ShortenerApi)).toBeDefined();
  });

  it("binds ShortenerRepository", () => {
    expect(shortenerContainer.get<ShortenerRepository>(SHORTENER_BINDINGS.ShortenerRepository)).toBeDefined();
  });

  it("binds GetUrlsUseCase", () => {
    expect(shortenerContainer.get<GetUrlsUseCase>(SHORTENER_BINDINGS.GetUrlsUseCase)).toBeDefined();
  });

  it("binds CreateUrlUseCase", () => {
    expect(shortenerContainer.get<CreateUrlUseCase>(SHORTENER_BINDINGS.CreateUrlUseCase)).toBeDefined();
  });
});
