export default {
  ShortenerApi: Symbol.for("UrlsApi"),
  ShortenerRepository: Symbol.for("ShortenerRepository"),
  RemoteShortenerRepository: Symbol.for("RemoteShortenerRepository"),
  GetUrlsUseCase: Symbol.for("GetUrlsUseCase"),
  CreateUrlUseCase: Symbol.for("CreateUrlUseCase"),
};
