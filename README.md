# Simple App UI

## Installation

### Prerequisites

**Important!** You will require both [Simple App UI](https://gitlab.com/matveikinner-public/assignments/hiq-finland-oy/simple-app-ui) and [Simple App Server](https://gitlab.com/matveikinner-public/assignments/hiq-finland-oy/simple-app-server) repositories from GitLab in the same parent directory to be able to launch / orchestrate all Docker containers required to run the application. You will also require Docker as the application uses PostgreSQL official [Dockerhub](https://hub.docker.com/_/postgres) container to "mock" database

- Simple App UI launches on localhost port 8080
- Simple App Server launches on localhost port 80

### Scripts

#### Makefile

To launch application containers with tiny shell CLI you can use Makefile `make` commands (if not on Unix system see [StackOverflow](https://stackoverflow.com/a/32127632/12660598))

To launch / orchestrate all Docker containers run

    make docker

Then in the CLI select

1. **Option 3** to "Compose"
2. **Option 1** to "Up"

#### Docker

To launch / orchestrate all Docker containers run

    docker-compose -f docker-compose.development.yml up

#### Yarn

_In case you want to ex. shut down UI or Server and run them locally yourself. However, the server database connection will still require PostgreSQL container running_

To install dependencies run

    yarn install

To run development version of the application with Webpack run

    yarn start:dev

To build production version of the application with Webpack run

    yarn build:prod

To launch production version of the application consider installing ex. http-server though Docker is de-factor way to run the application

## Solution

### Features

- React with architectural patterns such as [CLEAN Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) (with Inversify), [Flux Pattern](https://facebook.github.io/flux/docs/in-depth-overview/) (with Redux and Redux Saga) and [Repository Pattern](https://martinfowler.com/eaaCatalog/repository.html) (though only as ambiguous show-off as there is no ex. SqlJS to store data in more persistent manner). This architecture provides good separation between user interface, domain logic and data sources making it easier to reason about, maintain and refactor application layers in the long run
- Modern linter setup with ESLint and best practices which lints code styles, commit messages and runs tests
- Simple UX (with Material UI) including all basic features as per assignment
- Complete form validation
- Short URL binding history with expiration date visible
- Decent units tests with React Testing Library, Jest and Mock Service Worker
- Various scripts and deployment options to make it easy to run the application (though relies solely on Docker)

### Learnings

- Introducing RxJS into OOP mix with various architectural patterns and extracting values from stream(s) into generator functions / sagas
- Testing with React Testing Library and Mock Service Worker

### Caveats

- Latest Material UI v5 requires learning due to SX Props having bias to modify component styles directly among all the logic making it a bit more difficult to have good separation of concerns between component logic and styles
- Using Redux without additional tools such as ex. Redux Sauce results in a lot of boilerplate code
- An overkill architecture for such a small application (though there should always be consideration for the future development and extendability)

## Documentation

### Conventions

#### Commits

The [Commitlint](https://commitlint.js.org/#/) enforces best practices together with [Commitlint Jira rules](https://github.com/Gherciu/commitlint-jira). The commit message issue status must comply with [default Jira statuses](https://confluence.atlassian.com/adminjiracloud/issue-statuses-priorities-and-resolutions-973500867.html) which follow task ID (from Jira board or as running repository prefix and enumeration), task description (as abbreviation), and commit message which must be in present tense. These must all be in capital letters.

Format

    git commit -m "[<ISSUE-STATUS>]<TASK-ID>: [<TASK-DESCRIPTION>] <message>"

As an example

    git commit -m "[CLOSED]TASK-1: [CONF] Create .gitlab-ci.yml production branch CI / CD pipeline

##### Task descriptions

- [CONF] Informs that the ticket task description concerns configuration
- [FEAT] Informs that the ticket task description concerns feature
- [UPDT] Informs that the ticket task description concerns update to an existing feature / capability
- [FIX] Informs that the ticket task description concerns fix to a particular known issue / bug

_The option to include custom commit rules to include all possible scenarios is up to later consideration_

#### Branching Strategy

##### Overview

There are three main branches — development, staging, and production. These branches contain CI / CD pipeline which builds to corresponding domain / FQDN ex. https://\<branch>.\<domain>.\<domain-extension>

```mermaid

graph LR

T(Ticket) --> D(Development)

T(Ticket) --> F(Feature)

F --> D

D --> M(Master)

M --> S(Staging)

S --> P(Production)

```

##### Ticket

The ticket branch can branch out from either development branch or feature branch. Once complete the ticket branch merges always back to its parent branch. The only exceptions are unconventional situations where there is requirement to ex. cherry pick a particular commit higher in the tree to patch issues which require immediate attention. The ticket branch name derives from Jira board in use.

##### Feature

The feature branch can branch out only from the development branch. Once complete the feature branch merges always back to its parent branch. The feature branch name has "feature-" prefix which follows brief feature description ex. feature-password-reset.

##### Development

The development branch contains latest code.

##### Master

The master branch must contain at all times ready code ready for release to staging.

##### Staging

The staging branch must contain at all time ready code ready for release to production. Releases to this branch require appropriate version tag as a release candidate postfix and message.

Format

    git tag -a <semver>_rc<number> -m "<message>"

As an example

    git tag -a 1.0.0_rc1 -m "Release 1.0.0~rc1 to staging"

##### Production

The production branch contains latest official release. Releases to this branch require appropriate version tag and message.

Format

    git tag -a <semver> -m "<message>"

As an example

    git tag -a 1.0.0 -m "Release 1.0.0 to production"

#### Versions

The [Semantic Versioning](https://semver.org/) applies.
