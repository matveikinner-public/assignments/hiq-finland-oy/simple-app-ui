import coreContainer from "./coreContainer";
import CORE_BINDINGS from "./coreBindings";
import IHttpService from "@core/domain/interfaces/http.interface";

describe("Core DI Container", () => {
  beforeEach(() => {
    // create a snapshot so each unit test can modify it without breaking other unit tests
    coreContainer.snapshot();
  });

  afterEach(() => {
    // Restore to last snapshot so each unit test takes a clean copy of the application container
    coreContainer.restore();
  });

  it("binds HttpService", () => {
    expect(coreContainer.get<IHttpService>(CORE_BINDINGS.HttpService)).toBeDefined();
  });
});
