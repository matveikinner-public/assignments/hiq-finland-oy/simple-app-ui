import { CREATE_SUCCESS_TOAST, CREATE_ERROR_TOAST, RESET_TOAST } from "./toast.constants";
import { ToastActionTypes, ToastState } from "./toast.types";

const initialState: ToastState = {
  type: undefined,
  message: "",
};

const toastReducer = (state = initialState, action: ToastActionTypes): ToastState => {
  switch (action.type) {
    case CREATE_SUCCESS_TOAST:
      return {
        type: "success",
        message: action.payload,
      };
    case CREATE_ERROR_TOAST:
      return {
        type: "error",
        message: action.payload,
      };
    case RESET_TOAST:
      return initialState;
    default:
      return state;
  }
};

export default toastReducer;
