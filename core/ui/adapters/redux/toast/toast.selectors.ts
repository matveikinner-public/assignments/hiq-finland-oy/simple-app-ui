import { RootState } from "../../../frameworks/redux.config";
import { ToastState } from "./toast.types";

export const selectToastState = (state: RootState): ToastState => state.core.toast;
