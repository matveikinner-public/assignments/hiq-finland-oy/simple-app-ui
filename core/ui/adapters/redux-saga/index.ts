import { all } from "redux-saga/effects";
import shortenerRootSaga from "@shortener/ui/adapters/redux-saga";

export default function* rootSaga() {
  yield all([shortenerRootSaga()]);
}
