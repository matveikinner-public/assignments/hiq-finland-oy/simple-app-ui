import React from "react";
import { render, screen } from "@testing-library/react";
import * as reactRedux from "react-redux";
import CoreProvider from "@core/ui/app/CoreProvider";
import Loader from "./Loader";

describe("Loader", () => {
  const useSelectorMock = jest.spyOn(reactRedux, "useSelector");

  it("starts without loader", () => {
    const { container } = render(<Loader />, { wrapper: CoreProvider });

    /**
     * While testing component will never be null as RTL wraps components in single <div></div>
     * @see https://stackoverflow.com/a/62920582/12660598
     */
    expect(container).toBeEmptyDOMElement();
  });

  it("displays loader on state change", () => {
    useSelectorMock.mockReturnValue({ isActive: true });
    render(<Loader />, { wrapper: CoreProvider });

    const loader = screen.getByTestId("global-loader");
    expect(loader).toBeInTheDocument();
  });
});
