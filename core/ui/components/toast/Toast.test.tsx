import React from "react";
import { render, screen } from "@testing-library/react";
import * as reactRedux from "react-redux";
import Toast from "./Toast";
import CoreProvider from "@core/ui/app/CoreProvider";

describe("Toast", () => {
  const useSelectorMock = jest.spyOn(reactRedux, "useSelector");

  it("start without toast", () => {
    const { container } = render(<Toast />, { wrapper: CoreProvider });

    /**
     * While testing component will never be null as RTL wraps components in single <div></div>
     * @see https://stackoverflow.com/a/62920582/12660598
     */
    expect(container).toBeEmptyDOMElement();
  });

  it("displays toast on state change", () => {
    useSelectorMock.mockReturnValue({ type: "success" });
    render(<Toast />, { wrapper: CoreProvider });

    const loader = screen.getByTestId("global-toast");
    expect(loader).toBeInTheDocument();
  });
});
