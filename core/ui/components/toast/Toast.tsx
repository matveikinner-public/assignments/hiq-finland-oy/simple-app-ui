import { resetToast } from "@core/ui/adapters/redux/toast/toast.actions";
import { selectToastState } from "@core/ui/adapters/redux/toast/toast.selectors";
import { Alert, Snackbar } from "@mui/material";
import React, { FunctionComponent, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";

const Toast: FunctionComponent = () => {
  const [open, setOpen] = useState(false);

  const toast = useSelector(selectToastState);
  const dispatch = useDispatch();

  useEffect(() => {
    setOpen(true);
  }, [toast]);

  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
    dispatch(resetToast());
  };

  if (toast.type) {
    return (
      <Snackbar
        data-testid="global-toast"
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
      >
        <Alert severity={toast.type} onClose={handleClose}>
          {toast.message || "This is a success message!"}
        </Alert>
      </Snackbar>
    );
  }
  return null;
};

export default Toast;
