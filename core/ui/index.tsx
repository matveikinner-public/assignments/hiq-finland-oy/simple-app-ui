import "reflect-metadata";
import React from "react";
import ReactDOM from "react-dom";
import CoreModule from "./app/CoreModule";

ReactDOM.render(
  <React.StrictMode>
    <CoreModule />
  </React.StrictMode>,
  document.getElementById("root")
);
