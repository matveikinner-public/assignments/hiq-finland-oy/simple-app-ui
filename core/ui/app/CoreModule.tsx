import React, { FunctionComponent } from "react";
import { Container, createTheme } from "@mui/material";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import { v4 as uuid } from "uuid";
import CoreProvider from "./CoreProvider";
import routes from "./CoreModule.routes";

const App: FunctionComponent = () => {
  const theme = createTheme();
  return (
    <CoreProvider>
      <Container
        maxWidth="lg"
        sx={{
          height: "100vh",
          py: 7,
          px: 2,
          pb: 2,
          [theme.breakpoints.up("sm")]: {
            py: 8,
            px: 4,
            pb: 4,
          },
        }}
      >
        <Router>
          <Switch>
            {routes.map((route) => (
              <Route key={uuid()} {...route} />
            ))}
          </Switch>
          <Redirect to="/" />
        </Router>
      </Container>
    </CoreProvider>
  );
};

export default App;
