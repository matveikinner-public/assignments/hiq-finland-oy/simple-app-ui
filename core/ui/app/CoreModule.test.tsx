import React from "react";
import { render } from "@testing-library/react";
import { createMemoryHistory } from "history";
import CoreModule from "./CoreModule";
import CoreProvider from "./CoreProvider";

describe("CoreModule", () => {
  it("navigates to /", () => {
    render(<CoreModule />, { wrapper: CoreProvider });
    const history = createMemoryHistory();

    expect(history.location.pathname).toBe("/");
  });
});
