import React, { FunctionComponent, ReactNode } from "react";
import { Provider } from "react-redux";
import { createTheme, CssBaseline, ThemeProvider } from "@mui/material";
import store from "../frameworks/redux.config";
import customTheme from "../theme/custom.theme";

const CoreProvider: FunctionComponent<{ children: ReactNode }> = ({ children }) => {
  const theme = createTheme({ ...customTheme });
  return (
    <Provider store={store}>
      <CssBaseline />
      <ThemeProvider theme={theme}>{children}</ThemeProvider>
    </Provider>
  );
};

export default CoreProvider;
