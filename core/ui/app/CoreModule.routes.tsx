import { RouteProps } from "react-router-dom";
import SimpleModule from "@shortener/ui/app/Shortener";

const routes: RouteProps[] = [
  {
    path: "/",
    exact: false,
    children: SimpleModule,
  },
];

export default routes;
