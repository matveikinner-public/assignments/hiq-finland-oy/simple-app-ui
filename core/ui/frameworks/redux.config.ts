import { createStore, compose, applyMiddleware, combineReducers, CombinedState } from "redux";
import { connectRouter, routerMiddleware } from "connected-react-router/immutable";
import { createBrowserHistory, History } from "history";
import coreRootReducer from "../adapters/redux";
import shortenerRootReducer from "@shortener/ui/adapters/redux";
import sagaMiddleware from "./redux-saga.config";
import rootSaga from "../adapters/redux-saga";

/**
 * Fix to TypeScript, see
 * https://stackoverflow.com/a/52801110/12660598
 */
declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const history = createBrowserHistory();

const rootReducer = (_history: History) =>
  combineReducers({
    router: connectRouter(_history),
    core: coreRootReducer,
    shortener: shortenerRootReducer,
  });

export type RootState = ReturnType<CombinedState<ReturnType<typeof rootReducer>>>;

const store = createStore(
  rootReducer(history),
  composeEnhancers(applyMiddleware(routerMiddleware(history), sagaMiddleware))
);

sagaMiddleware.run(rootSaga);

export default store;
