import { ThemeOptions } from "@mui/material";
import globalTypography from "./typography/global.typography";

const customTheme: ThemeOptions = {
  typography: globalTypography,
};

export default customTheme;
