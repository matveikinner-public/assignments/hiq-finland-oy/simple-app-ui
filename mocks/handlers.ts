import { rest, DefaultRequestBody } from "msw";
import SHORTENER_RESOURCE_URLS from "@shortener/data/network/api/shortenerResourceUrls";
import UrlsNetwork from "@shortener/data/network/models/urlsNetwork";

const mockBaseUrl = "http://localhost";

const mockUrls: UrlsNetwork.Url[] = [
  {
    id: "c55cb09c-de70-4525-973a-563663af81e0",
    originalUrl: "https://hiqfinland.fi/avoimet-tyopaikat/",
    tinyUrl: "http://localhost:3000/945ecbd8956cf387",
    validUntil: "2021-11-04T22:36:36.139Z",
    createdAt: "2021-10-28T18:36:36.152Z",
    updatedAt: "2021-10-28T18:36:36.152Z",
  },
];

const handlers = [
  rest.get<DefaultRequestBody, UrlsNetwork.GetResponse>(
    `${mockBaseUrl}/${SHORTENER_RESOURCE_URLS.CRUD.URLS}`,
    (req, res, ctx) => {
      return res(
        ctx.status(200),
        ctx.json({
          statusCode: 200,
          data: mockUrls,
        })
      );
    }
  ),
];

export default handlers;
